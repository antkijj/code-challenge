/*
 * Program demonstruje zastosowanie operatorow ++ i --
 * 
 * Copyright 2020 Unknown <play@work-80ru>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <iostream>
#include <iomanip>

using namespace std;


int main() {
	int num = 4; // Wartosc poczatkowa wynosi 4
	
	// Wyswietelnie wartosci num
	cout << "Wartosc zmiennej num to " << num << "." << endl;
	cout << "Teraz dokonam inkrementacji num.\n\n";
	
	//Postinkrementacja num za pomoca ++ 
	num++;
	cout << "Teraz wartosc num wynosi " << num << "." << endl;
	cout << "Dokonam ponownej inkrementacji.\n\n";
	
	//Preinkrementacja num za pomoca++
	++num;
	cout << "Teraz wartosc num wynosi " << num << "." << endl;
	cout << "Dokonam dekrementacji num.\n\n";
	
	//Postinkrementacja num za pomoca --
	num--;
	cout << "Teraz wartosc num wynosi " << num << "." << endl;
	cout << "Dokonam ponownej dekrementacji.\n\n";
	
	//Predekrementacja num za pomoca --
	--num;
	cout << "Teraz wartosc num wynosi " << num << "." << endl;
	return 0;
}
 

