/*
 * This program is using modulo to estimate
 * if number is even or not if is it divide by 2 with not fraction.
 * Fraction is a signal about number is not even
 * 
 * 
 * Copyright 2020 Unknown <play@work-80ru>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <iostream>
#include <iomanip>

using namespace std;


int main() {
	int number;
	
	cout << "Output integer and i will tell you\n";
	cout << "If is even or not. ";
	cin >> number;
	if (number % 2 == 0)
		cout << number << " is even.\n";
	else
		cout << number << " is not even.\n";
	return 0;
} 

