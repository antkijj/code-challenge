//Ten program demonstruje zastosowanie funkcji z trzema parametrami
#include <iostream>

using namespace std;
//Prototyp funkcji
void showSum(int, int, int);

int main() {
    int value1, value2, value3;

    //Pobranie trzech liczb calkowitych
    cout << "Wpisz trzy liczby calkowite, a ja";
    cout << "wyswietle ich sume: ";
    cin >> value1 >> value2 >> value3;

    //Wywolanie showSum() z przekazaniem trzech argumentow
    showSum(value1, value2, value3);
    return 0;
}

//Definicja funkcji showSum()
//Funkcja ta uzywa trzech parametrow typu int. Wyswietla ich sume

void showSum(int num1, int num2, int num3)
{
    cout << (num1 + num2 + num3) << endl;
}
