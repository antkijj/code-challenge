/*
 * Bool print true or false
 * 
 * 
 * Copyright 2020 Unknown <play@work-80ru>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <iostream>
#include <iomanip>

using namespace std;


int main() {
	int score1, score2, score3; //Store 3 results of test
	double average; //Store average
	
	//Output 3 results
	cout << "Type 3 results of the test and i will give the average: ";
	cin >> score1 >> score2 >> score3;
	
	//Estimate and print average
	average = (score1 + score2 + score3) / 3.0;
	cout << fixed << showpoint << setprecision(1);
	cout << "Your average is " << average << "." << endl;
	
	//Congrats
	if (average == 100)
		cout << "Congrats! Perfect score\n";
	return 0;	
} 

