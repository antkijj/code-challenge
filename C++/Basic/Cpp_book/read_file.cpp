/*
 * //Ten program odczytuje dane liczbowe z pliku
 * 
 * Copyright 2020 Unknown <play@work-80ru>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char **argv)
{
	ofstream outputFile;	//Obiekt strumienia pliku
	int numberOfDays;		//Liczba dni
	double sales;			//Wartosc sprzedazy za dzien
	
	//Pobranie liczby dni
	cout << "Z ilu dni chcesz wprowadzic sprzedaz? ";
	cin >> numberOfDays;
	
	// Otwarcie pliku Sprzedaz.txt
	outputFile.open("Sprzedaz.txt");
	
	//Pobranie sprzedazy za kazdy dzien i zapisanie
	//jej wartosci w pliku
	for (int count = 1; count <= numberOfDays; count++)
	{
		//Pobranie wartosci sprzedazy za dany dzien
		cout << "Wpisz wartosc sprzedazy za dzien "
			 << count << ": ";
		cin >> sales;
		//Zapisanie sprzedazy w pliku
		outputFile << sales << endl; 	 
	}
	
	//Zamkniecie pliku
	outputFile.close();
	cout << "Dane zapisano w Sprzedaz.txt.\n";
	return 0;	 
	
}

