/*
 *Program prosi o wpisanie dowoch liczb: num1 i num 2
 * num1 jest dzielone przez num2 a wynik jest wyswietalny na ekranie
 * Jednak przed dzieleniem num2 jest sprawdzane
 * na wartosc 0 jezeli zawiera dzielenie
 * nie nastapi
 * 
 * 
 * Copyright 2020 Unknown <play@work-80ru>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <iostream>
#include <iomanip>

using namespace std;


int main() {
	double num1, num2, quotient;
	
	//Wypisanie pierwszej liczby
	cout << "Wpisz liczbę: ";
	cin >> num1;
	
	//Wpisanie drugiej liczby
	cout << "Wpisz kolejną liczbę: ";
	cin >> num2;
	
	//Jeśli num 2 nie jest zerem wykonanie dzielenia
	if (num2 == 0)
	{
		cout << "Dzielenie przez zero nie jest mozliwe.\n";
		cout << "Uruchom ponownie program i wpisz\n";
		cout << "liczbe inna niz zero.\n";
	}
	else
	{
		quotient = num1 / num2;
		cout << "Iloraz liczby " << num1 << " podzielonej przez ";
		cout << num2 << " wynosi " << quotient << ".\n";
	}
	return 0;
} 

