/*
 * Nested IF
 * 
 * Copyright 2020 Unknown <play@work-80ru>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <iostream>
#include <iomanip>

using namespace std;


int main() {
	char employed,	//Obecnie zatrudniony, T or N
		recentGrad; // Swiezy absolwent,   T or N
	
	//Czy uzytkownik jest zatrudniony oraz czy jest swiezym absolwentem?
	cout << "Odpowiedz na pytania,\n";
	cout << "wpisujac T jako Tak lub ";
	cout << "N jako NIE.\n";
	cout << "Czy jesteś zatrudniony? ";
	cin >> employed;
	cout << "Czy ukonczyles szkole wyzsza ";
	cout << "w ciagu ostatnich dwoch lat? ";
	cin >> recentGrad;
	
	//Sprawdzenie, czy uzytkownik kwalifikuje sie do pozyczki
	if (employed == 'T')
	{
		if (recentGrad == 'T') //Zagnizdzony if
		{
			cout << "Kwalifikujesz sie do uzyskania ";
			cout << "obnizonago oprocentowania kredytu.\n";
		}
		else //Zatrudniony ale nie swiezy absolwent
		{
			cout << "Musisz miec ukoczony szkole wyzsza ";
			cout << "w ciagu ostatnich dwoch lat.\n";
			cout << "aby sie zakwalifikowac.\n";
		}	
	}
	else // Niezatrudniony
	{
		cout << "Musisz byc zatrudniony aby byc zakalifikowanym.\n";
	}		
	return 0;
}
 

