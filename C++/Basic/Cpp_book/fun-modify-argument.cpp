//Ten program pokazuje ze modyfikacje parametru
//nie wplywaja na argument
#include <iostream>

using namespace std;
//Prototyp funkcji
void changeMe(int);

int main() {
    int number = 12;

    //Wyswietlenie wartosci zmiennej number
    cout << "Wartosc number wynosi" << number << "." << endl;

    //Wywolanie changeMe() z przekazaniem wartosci z number
    //jako argumentu
    changeMe(number);

    //Wyswietlenie wartosci zmiennej number ponownie
    cout << "Wracam do metody main(), wartosc ";
    cout << "number wynosi" << number << "." << endl;
    return 0;
}

//Definicja funkcji changeMe()
//Ta funkcja zmienia wartosc parametru

void changeMe(int myValue)
{
    //Zmiana wartosci myValue na 0
    myValue = 0;

    //Wyswietlenie wartosci z myValue
    cout << "Teraz wartosc wynosi " << myValue << "." << endl;
}
