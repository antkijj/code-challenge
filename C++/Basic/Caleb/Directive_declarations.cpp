/*
 * Variable Declaration and Initialization
 * Best practices
 * pow functions
 * custom functions
 * void functions
 *
 * Copyright 2020 Unknown <play@work-80ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <iostream>
#include <cmath>


double power(double , int ); //declaration

/*
 * double power(double base, int exponent) //no need up declaration
{
		return 0.0;
}
*/

void print_pow(double base, int exponent)
{
  double myPower = power(base, exponent);
  std::cout << base << " raised to the" << exponent << "power is" << myPower << ".\n";
}

int main() {

  int slices; //declaration
  std::cout << "YO fatty how many pieces of pizza you eat?: ";
  std::cin >> slices;
  //slices = 5; //initialization

  std::cout << "You Have " << slices << std::endl;

  double base;
  int exponent;
  //int base, exponent;
  std::cout << "What is the base?: ";
  std::cin >> base;
  std::cout << "What is the exponent?";
  std::cin >> exponent;
  //std::cout << pow(base, exponent);
  //double power = pow(base, exponent);
  //double myPower = power(base, exponent);
  //std::cout << myPower << std::endl;
  print_pow(base, exponent);
}

double power(double base, int exponent) //definition
{
		double result = 1;
		for (int i = 0; i < exponent; i++)
		{
			result = result * base;
		}
		return result;
}
