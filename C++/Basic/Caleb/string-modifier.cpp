/*
 * String modifer methods
 * String operation methods
 * 
 * Copyright 2020 work <work@work-80ru>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>

int main(int argc, char **argv)
{
	std::string greeting = "What the hell?";
    //greeting.append("there");
    //greeting.insert(3, "    ");
    //greeting.erase(greeting.length() -1);
    //greeting.pop_back();
    //greeting.replace( greeting.find("hell"), 4, "$$$$");
    //std::cout << greeting.substr(5 ,2) << std::endl;
    std::cout << greeting.find_first_of("!") << std::endl;
    //npos == -1
    //unsigned long x = -1;
    //std::cout << x << std::endl;
    // if(greeting.find_first_of("!") == -1) std::cout << "NOT FOUND!" << std::endl;
    if (greeting.compare("What is up?") == 0) std::cout << "Equals" << std::endl;
    //std::cout << greeting << std::endl;
}

