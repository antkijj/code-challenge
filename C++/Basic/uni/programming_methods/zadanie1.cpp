//Metody programowania zadanie 1

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct Student{
    string imie, nazwisko;
    long numerIndeksu;
};

struct wykaz{
    int rozmiar;
    Student *lista;
};

//Protoyp fun
wykaz wczytaj(ifstream &plik);
void show(const wykaz *Swykaz);

int main() {

    ifstream plik ("/home/work/CLionProjects/dump/stud");
    if(!plik.good())
    {
        cout << "Error" << endl;
    }

    wykaz Swykaz = wczytaj(plik);
    show(&Swykaz);
    delete [] Swykaz.lista;
    return 0;
}

wykaz wczytaj(ifstream &plik)
{
    struct wykaz Swykaz;
    if(plik.good())
    {
        plik >> Swykaz.rozmiar;
        Swykaz.lista = new Student[Swykaz.rozmiar];
    }
    for(int i = 0; i < Swykaz.rozmiar ;i++) {
        plik >> Swykaz.lista[i].imie;
        plik >> Swykaz.lista[i].nazwisko;
        plik >> Swykaz.lista[i].numerIndeksu;

    }
    return Swykaz;
}

void show(const wykaz *Swykaz)
{
    cout << Swykaz->rozmiar << "\n";

    for (int i =0; i < Swykaz->rozmiar;i++)
    {
        cout << Swykaz->lista[i].imie << " " << Swykaz->lista[i].nazwisko << " " << Swykaz->lista[i].numerIndeksu << "\n";

    }
}